//max=INT_MIN
//pentru toate valorile x de parcurs
//if (x > max){
//	max = x;
//}

#include <iostream>
using namespace std;

int main() {
	int n = 1234, max;

	cin >> n;
	max = INT_MIN;
	for (int t = n; t > 0; t /= 10) {
		if (t % 10 > max) {
			max = t % 10;
		}
	}

	cout << max;
}
